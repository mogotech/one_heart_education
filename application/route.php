<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;


//文章分类
Route::post('api/category','yxsy/category/index');
//文章
Route::post('api/article/index','yxsy/article/index');//列表
Route::post('api/article/detail','yxsy/article/read');//详情
Route::post('api/article/poster','yxsy/article/poster');//生成海报
Route::get('api/article/test','yxsy/article/test');//生成海报

//报名
Route::post('api/enroll','yxsy/enroll/save');
Route::post('api/enroll/shuju','yxsy/enroll/getAll');
Route::get('enroll/baoming','yxsy/enroll/baoming');

//miss
//Route::miss('yxsy/base/miss');