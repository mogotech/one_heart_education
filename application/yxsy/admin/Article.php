<?php
namespace app\yxsy\admin;

use app\common\builder\ZBuilder;
use app\yxsy\model\Article as ArticleModel;
use think\Db;
use app\yxsy\model\Category as CategoryModel;

/**
 * 文章
 * @author lan
 */
class Article extends Base
{
    public function index(){
        // 获取查询和排序规则
        $map = $this->getMap();
        $order = $this->getOrder();
        $data = Db::view('yxsy_article')
            ->view('yxsy_category','name','yxsy_category.id=yxsy_article.type_id')
            ->where($map)
            ->order($order)
            ->order('id desc')
            ->paginate()->each(function($item){
                // 对分页数据可以做额外处理
//            $row=Db::table('dp_lmsx_category')->where('id',$item['type_id'])->field('name')->find();
//            $item['type_id']=$row['name'];
                $item['content']=msubstr($item['content'],20);
                //dump($item);die;
                return $item;
            });
        return ZBuilder::make('table')
            ->setTableName('lmsx_article')  //需要修改 这里修改正确可以直接修改字段 删除
            ->addColumns([
                ['id','id'],
                ['title','文章标题'],
                ['icon','封面图片','picture'],
                ['name','文章分类'],
                ['come_from','来源'],
                //['view','浏览量'],
                //['content','文章内容'],
                ['create_time','创建时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('add', [],[])
            ->addRightButton('detail', ['icon' => 'fa fa-fw fa-eye', 'title' => '查看详情', 'href' => url('detail', ['id' => '__id__'])])
            ->addRightButton('edit',[],[])
            ->addTopButtons(['delete'])
            ->setRowList($data)
            ->addFilter(['name'=>'yxsy_category'])
            ->setSearch(['id' => 'ID', 'title' => '标题'])
            ->fetch();
    }
    public function add(){
        if($this->request->isPost()){
            // 第一步 获取数据 如果后台面向大众 需要用only方法
            $data = request()->post();
            // 第二部 验证数据
            $result = $this->validate($data,'Article');
            if($result!==true) $this->error($result);
            // 第三部 存储
            $res=ArticleModel::create($data);
            $this->success('保存成功', null, '_parent_reload');
        }
        return ZBuilder::make('form')
            ->addFormItems([
                ['text', 'title', '文章标题','必须'],
                ['image', 'icon', '封面图片','必须,图片大小限2M'],
                ['select', 'type_id', '文章分类','必须',CategoryModel::getTreeList()],
                ['ueditor', 'content', '文章内容','必须'],
                ['text', 'come_from', '来源',''],
                //['number', 'view', '浏览量']
            ])
            ->fetch();
    }
    public function edit($id=null){
        if($this->request->isPost()){
            $data = request()->post();
            $result = $this->validate($data,'Article');
            if($result!==true) $this->error($result);
            ArticleModel::update($data);
            $this->success('保存成功', null, '_parent_reload');
        }
         $data = ArticleModel::get($id);

        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden','id'],
                ['text', 'title', '标题','必须'],
                ['image', 'icon', '文章封面图片','必须,图片大小限2M'],
                ['select', 'type_id', '文章分类','必须',CategoryModel::getTreeList()],
                ['ueditor', 'content', '文章内容','必须'],
                ['text', 'come_from', '来源',''],
                //['number', 'view', '浏览量']
            ])
            ->setFormData($data)
            ->fetch();
    }

    /**
     * 查看详情
     */
    public function detail($id=null){
        if (empty($id)) $this->error('参数错误');
        $info=Db::view('dp_yxsy_article a')
            ->view('yxsy_category c','name','c.id=a.type_id','left')
            ->where('a.id',$id)
            ->find();
        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('编辑') // 设置页面标题
            ->addFormItems([ // 批量添加表单项
                ['hidden', 'id'],
                ['static', 'title', '标题'],
                ['static', 'name', '文章分类'],
                ['ueditor', 'content', '文章内容'],
                //['static', 'view', '浏览量']
            ])
            ->hideBtn('submit')
            ->setFormData($info) // 设置表单数据
            ->fetch();
    }


}