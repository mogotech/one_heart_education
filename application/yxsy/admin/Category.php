<?php
namespace app\yxsy\admin;

use app\common\builder\ZBuilder;
use app\yxsy\model\Category as CategoryModel;
use think\Db;

class Category extends Base {
    public function index(){
        // 获取查询和排序规则
        $map = $this->getMap();
        $order = $this->getOrder();
        $data = Db::table('dp_yxsy_category',true) //TODO 需要修改
            ->where($map)
            ->order($order)
            ->order('id desc')
            ->paginate()->each(function($item){
                // 对分页数据可以做额外处理
                return $item;
            });
        return ZBuilder::make('table')
            ->setTableName('yxsy_category')  //需要修改 这里修改正确可以直接修改字段 删除
            ->addColumns([
                ['id','id'],
                ['name','分类名称'],
                ['status','是否可见','switch'],
                ['sort','排序'],
//                ['sort','排序','number'],
                ['create_time','创建时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->addTopButton('add', [],[])
            ->addRightButton('edit',[],[])
            ->addTopButtons(['delete'])
            ->setRowList($data)
//            ->addFilter([ 'username'])
            ->setSearch(['id' => 'ID', 'name' => '分类名称'])
            ->fetch();
    }
    public function add(){
        if($this->request->isPost()){
            // 第一步 获取数据 如果后台面向大众 需要用only方法
            $data = request()->post();
            // 第二部 验证数据
            $result = $this->validate($data,'Category');
            if($result!==true) $this->error($result);
            // 第三部 存储
            CategoryModel::create($data);
            $this->success('保存成功', null, '_parent_reload');
        }
        return ZBuilder::make('form')
            ->addFormItems([
                ['text', 'name', '分类名称'],
                ['number', 'sort', '排序','',0],
                ['radio', 'status', '是否可见','',['否','是'],1],
            ])
            ->fetch();
    }
    public function edit($id=null){
        if($this->request->isPost()){
            $data = request()->post();
            $result = $this->validate($data,'Category');
            if($result!==true) $this->error($result);
            CategoryModel::update($data);
            $this->success('保存成功', null, '_parent_reload');
        }
        $data = CategoryModel::get($id);
        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden','id'],  // 多增加一个影藏ID 即可
                ['text', 'name', '分类名称'],
                ['number', 'sort', '排序'],
                ['radio', 'status', '是否可见','',['否','是']],
            ])
            ->setFormData($data)
            ->fetch();
    }

//    public function delete($record = [])
//    {
//        $res=CategoryModel::destroy(['id'=>['in',$record]]);
//        if($res)$this->success('删除成功');
//
//    }
}