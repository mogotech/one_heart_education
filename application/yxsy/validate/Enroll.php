<?php

namespace app\yxsy\validate;
use think\Validate;
class Enroll extends Validate
{
    //定义验证规则
    protected $rule = [
        'grade|年级'  => 'require',
        'subject|课程'  => 'require',
        'phone|手机'  => 'require|regex:^1[3,5,6,7,8]{1}\d{9}$',

    ];

    //定义验证提示
    protected $message = [
        'phone.regex' => '手机号码错误',
    ];


}
