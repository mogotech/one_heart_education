<?php

namespace app\yxsy\validate;
use think\Validate;
class Category extends Validate
{
    //定义验证规则
    protected $rule = [
        'name|分类名称'  => 'require',
        'sort|排序'  => 'regex:^[0-9]\d{0,}$',

    ];

    //定义验证提示
    protected $message = [
//        'name.regex' => '行为标识由字母和下划线组成',
        'sort.regex' => '排序必须是大于0的整数',
    ];


}
