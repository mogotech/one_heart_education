<?php

namespace app\yxsy\validate;
use think\Validate;
class Article extends Validate
{
    //定义验证规则
    protected $rule = [
        'title|文章标题'  => 'require',
        'icon|封面图片'  => 'require',
        'type_id|类型' => 'require',
        'come_from|来源' => 'require',

    ];

    //定义验证提示
    protected $message = [
    ];
}
