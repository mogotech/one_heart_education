<?php
namespace app\yxsy\model;

use think\Model;
use traits\model\SoftDelete;
use util\Tree;
use think\Db;

//分类模型
class Category extends Model
{
    use SoftDelete;
    protected $table = 'dp_yxsy_category';
    protected $autoWriteTimestamp = true;



    public static function getTreeList()
    {
        $list=self::where(['status'=>1,'delete_time'=>null])->column('id,name');
        if(empty($list)) return $list[0]='暂无可用分类';
        return $list;
    }

}
