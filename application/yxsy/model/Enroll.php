<?php
namespace app\yxsy\model;

use think\Model;
use traits\model\SoftDelete;

//报名模型
class Enroll extends Model
{
    use SoftDelete;
    protected $table = 'dp_yxsy_enroll';
    protected $autoWriteTimestamp = true;
}
