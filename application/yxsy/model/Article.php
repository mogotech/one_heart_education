<?php
namespace app\yxsy\model;

use think\Model;
use traits\model\SoftDelete;

//文章模型
class Article extends Model
{
    use SoftDelete;
    protected $table = 'dp_yxsy_article';
    protected $autoWriteTimestamp = true;
}
