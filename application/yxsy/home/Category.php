<?php
namespace app\yxsy\home;

use app\yxsy\model\Category as CategoryModel; // 与控制器同名 重命名
class Category extends Base {
    // 获取列表
    public function index($row=10){
        // 查询字段
        $map = $this->getMap();
        $map['status']=1;
        $Post = new CategoryModel();
        // 自定义where查询
        $data =  $Post
            ->order('sort desc,id desc')
            ->where($map)
            ->paginate($row)
            ->each(function($item){
                return $item;
            });
        $data = $data->toArray();
        return mogo_json($data);
    }
}