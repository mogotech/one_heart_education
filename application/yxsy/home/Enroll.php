<?php
namespace app\yxsy\home;

use app\yxsy\model\Enroll as EnrollModel; // 与控制器同名 重命名
class Enroll extends Base {
    // 获取列表
    public function index($row=10){
        // 查询字段
        $map = $this->getMap();
        $Post = new EnrollModel();
        // 自定义where查询
        $map['type']='skill';
        $data =  $Post
            ->order('id desc')
            ->where($map)
            ->paginate($row)
            ->each(function($item){
                //对item增加额外字段
                $item['diy'] = 'diy';
                return $item;
            });
        $data = $data->toArray();
        // 增加额外数据
        return json($data);
    }
    // 保存内容
    public function save(){
        // 第一步 过滤字段
        $data = request()->only(['subject', 'grade','phone','name','glass']);
        // 第二部 验证字段
        $result = $this->validate($data,'Enroll');
        if(true!==$result) return mogo_error($result,404);
        // 第三部 存储字段
        $res = EnrollModel::create($data);
        // 第四部 返回结果
        return mogo_json($res,'ok',1);
    }

    //获取报名页面的初始数据
    public function getAll()
    {
        $arr=[];
        $a=config('grade');
        $b=config('subject');
        $c=config('yxsy_up_img');
        $c=get_file_path($c);
        $d=config('yxsy_down_img');
        $d=get_file_path($d);
        $arr=['grade'=>$a,'subject'=>$b,'yxsy_up_img'=>$c,'yxsy_down_img'=>$d];
        return mogo_json($arr,'ok',1);
    }

    //报名界面
    public function baoming()
    {
        return $this->fetch();
    }
}