<?php
namespace app\yxsy\home;

use app\yxsy\model\Article as ArticleModel; // 与控制器同名 重命名
class Article extends Base {
    // 获取列表
    public function index(){
        $search=request()->only(['type_id','row']);
        if(!isset($search['row'])){
            $search['row']=1;
        }
        // 查询字段
        $map = $this->getMap();
        if(!isset($search['type_id'])||empty($search['type_id'])||!is_numeric($search['type_id']))
            return mogo_error('参数错误',404);
        $map['yxsy_article.type_id']=$search['type_id'];
        $Post = new ArticleModel();
        $config=[
            'page'=>$search['row'],
        ];
        $listrows=config('default_more');
        if(empty($listrows))$listrows=10;
        // 自定义where查询
        $data =  $Post::view('yxsy_article')
            ->view('yxsy_category','name','yxsy_category.id=yxsy_article.type_id','left')
            ->order('id desc')
            ->where($map)
            ->paginate($listrows,false,$config)
            ->each(function($item){
                $item['update_time']=format_time($item['update_time']);
                $item['icon']=get_images_all($item['icon']);
                $item['title']=msubstr($item['title'],20);
                return $item;
            });

        $data = $data->toArray();
        //去除第一条重复数据
        $count=count($data['data']);
        if($count>=1){
            unset($data['data'][0]);
        }
        //halt($data);
            $x =  $Post::view('yxsy_article')
                ->view('yxsy_category','name','yxsy_category.id=yxsy_article.type_id','left')
                ->order('id desc')
                ->where('yxsy_article.type_id',$search['type_id'])
                ->paginate()
                ->each(function($item){
                    $item['update_time']=format_time($item['update_time']);
                    $item['icon']=get_images_all($item['icon']);
                    $item['title']=msubstr($item['title'],20);
                    return $item;
                });
            $x=$x->toArray();
            if(empty($x['data'])){
                $m=$x['data'];
            }else{
                $m=$x['data'][0];
            }

            $a=[];
            foreach ($data['data'] as $v){
                $a[]=$v;
            }
        // 增加额外数据
        return mogo_json($a,'ok',$m);
    }
   
    // 读取内容
    public function read($id){
        $data = ArticleModel::get($id);
        // 第一步 验存
        if(!$data) return mogo_error('文章不存在',404);
        // 这里可以做一些处理
        $data['update_time']=format_time($data['update_time']);
        $data['content']=get_path_img($data['content']);
        $data['title']=msubstr($data['title'],20);

        return mogo_json($data);
    }

    //生成海报
    public function poster($id='')
    {
        $data = ArticleModel::get($id);
        // 第一步 验存
        if(!$data) return mogo_error('文章不存在',404);
        //生成小程序码
            $appid=config('weiXin')['appid'];
            $secret=config('weiXin')['secret'];
            $url1="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$secret";
            $access_token = json_decode(get_curl($url1), true)['access_token'];
            $page="pages/index/detail";
            $page="pages/index/index";
            $scene = 'scene';
            $name='yxsy'.'.jpg';
            $width=430;
            $post_data='{"scene":"'.$scene.'","page":"'.$page.'","width":'.$width.'}';
            $url="https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=".$access_token;
            $result=api_notice_increment($url,$post_data);
            if(!file_exists($name)){
                file_put_contents($name, $result);
            }
        $url='https://yixinjiaoyu.mogochina.com'.'/yxsy'.'.jpg';
        return mogo_json($url,'ok',1);
      //  echo '<img src="'.$url.'" />';

    }

//    public function test()
//    {
//        //header('content-type:image/gif');
//        $appid=config('weiXin')['appid'];
//        $secret=config('weiXin')['secret'];
//        $url1="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$secret";
//        $access_token = json_decode(get_curl($url1), true)['access_token'];
//
////        $page="pages/index/detail";
//        $page="pages/index/index";
//        $scene = 'scene';
//        $name='yxsy'.'.jpg';
//        $width=430;
//        $post_data='{"scene":"'.$scene.'","page":"'.$page.'","width":'.$width.'}';
//       // halt($post_data);
//        $url="https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=".$access_token;
//        $result=api_notice_increment($url,$post_data);
//        if(!file_exists($name)){
//            file_put_contents($name, $result);
//        }
//        echo $result;
//    }
   
}