<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/29
 * Time: 17:33
 */
/**
 * 模块信息
 */
return [
    // 模块名[必填]
    'name'        => 'yxsy',
    // 模块标题[必填]
    'title'       => '壹心书院',
    // 模块唯一标识[必填]，格式：模块名.开发者标识.module
    'identifier'  => 'yxsy.lan.module',
    // 模块图标[选填]
    'icon'        => 'fa fa-fw fa-newspaper-o',
    // 模块描述[选填]
    'description' => '壹心书院模块',
    // 开发者[必填]
    'author'      => 'lan',
    // 开发者网址[选填]
    'author_url'  => 'http://www.dolphinphp.com',
    // 版本[必填],格式采用三段式：主版本号.次版本号.修订版本号
    'version'     => '1.0.0',

];